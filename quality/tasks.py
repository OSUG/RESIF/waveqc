import math
import platform
from http.client import IncompleteRead
from pathlib import Path

import pendulum
from celery import chain, group, shared_task
from celery.app.task import Task
from celery.utils.log import get_task_logger
from django.conf import settings
from obspy import UTCDateTime, read
from obspy import __version__ as obspy_version
from obspy.clients.fdsn import Client
from obspy.clients.fdsn.client import (
    FDSNBadRequestException,
    FDSNException,
    FDSNNoDataException,
)
from obspy.clients.fdsn.header import platform_
from obspy.core.inventory import read_inventory
from obspy.core.stream import Stream
from obspy.io.mseed import InternalMSEEDError

from .models import (
    Channel,
    Check,
    Network,
    Operator,
    Station,
)

logger = get_task_logger(__name__)


# We request waveforms for 1 day (86400 seconds)
CHECK_TIME_REACH = 86400

# Used when decimating waveforms
TARGET_SAMPLING_RATE = 5

# Channel naming
# See : https://www.fdsn.org/pdf/SEEDManual_V2.4.pdf p. 133
BAND_CODE = 0
INSTRUMENT_CODE = 1

# High frequency channels according to channel naming's band code
HIGH_FREQUENCY_CHANNELS = "FGDCESHB"

# Seismometer channels according to channel naming's instrument code
SEISMOMETER_CHANNELS = "HLGMN"


def get_obspy_client() -> Client:
    user_agent = (
        f"WaveQC (ObsPy/{obspy_version}, {platform_}, "
        f"Python {platform.python_version()})"
    )
    return Client(
        settings.WAVEQC_FDSN_CLIENT, user_agent=user_agent, _discover_services=False
    )


@shared_task(
    ignore_results=True, autoretry_for=(Exception,), retry_backoff=10, retry_jitter=True
)
def update_inventory(date: str) -> None:
    client = get_obspy_client()
    networks = ",".join([network.code for network in Network.objects.only("code")])
    inventory = client.get_stations(network=networks, level="station")
    # We need to update all stations first to get an end_date if available
    Network.objects.populate(inventory)
    Station.objects.populate(inventory)
    Operator.objects.populate(inventory)
    Operator.objects.link_to_stations(inventory)
    Operator.objects.purge_obsoletes()
    target = UTCDateTime(date)
    inventory = client.get_stations(
        network=networks, level="channel", startbefore=target
    )
    Channel.objects.populate(inventory)


@shared_task(autoretry_for=(Exception,), retry_backoff=10, retry_jitter=True)
def build_station_list(date: str) -> list[str]:
    day = pendulum.parse(date)
    stations = Station.objects.opened(day=day).select_related("network")  # type: ignore[arg-type]
    return [f"{station.network.code}_{station.code}" for station in stations]


@shared_task(ignore_results=True)
def download_stationxml(station: str, date: str) -> None:
    client = get_obspy_client()
    target = UTCDateTime(date)
    network, station = station.split("_")
    try:
        client.get_stations(
            network=network,
            station=station,
            level="response",
            startbefore=target,
            endafter=target,
            filename=(
                f"{settings.WAVEQC_STATIONXML_STORAGE_PATH}/{network}_{station}.xml"
            ),
        )
    except FDSNNoDataException as _:
        pass
    except FDSNBadRequestException as _:
        logger.warning("Bad request : %s", station)
    except (FDSNException, IncompleteRead, ValueError) as _:
        logger.warning("Web Service is not responding : %s", station)
    # This last exception shouldn't be here. Obspy should return one of its exceptions
    # Once obspy 1.5.0 is release, remove this
    except AttributeError as _:
        logger.warning("Web Service is not responding : %s", station)


@shared_task(bind=True)
def download_stationxmls(self: Task, stations: list[str], date: str) -> object:
    return self.replace(
        group([download_stationxml.si(station, date) for station in stations]),
    )


@shared_task(autoretry_for=(Exception,), retry_backoff=10, retry_jitter=True)
def build_check_list(date: str) -> list[dict[str, str]]:
    day = pendulum.parse(date)
    channels = Channel.objects.opened(day=day).select_related("station__network")  # type: ignore[arg-type]
    return [
        {
            "channel": channel.code,
            "location": channel.location,
            "station": channel.station.code,
            "network": channel.station.network.code,
            "date": date,
        }
        for channel in channels
    ]


@shared_task(autoretry_for=(Exception,), retry_backoff=10, retry_jitter=True)
def get_failed_checks() -> list[dict[str, object]]:
    failed_checks = Check.objects.failed().select_related("channel__station__network")
    return [
        {
            "channel": check.channel.code,
            "location": check.channel.location,
            "station": check.channel.station.code,
            "network": check.channel.station.network.code,
            "date": check.date,
        }
        for check in failed_checks
    ]


@shared_task
def download_mseed(check: dict[str, str]) -> str:
    client = get_obspy_client()
    start = UTCDateTime(check["date"])
    end = start + CHECK_TIME_REACH
    filename = (
        f"{settings.WAVEQC_MSEED_STORAGE_PATH}/"
        f"{check['network']}."
        f"{check['station']}."
        f"{check['location']}."
        f"{check['channel']}__"
        f"{start.strftime('%Y%m%dT%H%M%SZ')}__"
        f"{end.strftime('%Y%m%dT%H%M%SZ')}"
        ".mseed"
    )

    try:
        client.get_waveforms(
            check["network"],
            check["station"],
            check["location"],
            check["channel"],
            start,
            end,
            filename=filename,
        )
    except FDSNNoDataException as _:
        pass
    except FDSNBadRequestException as _:
        logger.warning(
            "Bad request : %s",
            ".".join([str(value) for value in check.values()]),
        )
    except (FDSNException, IncompleteRead, ValueError) as _:
        logger.warning(
            "Web Service is not responding : %s",
            ".".join([str(value) for value in check.values()]),
        )
    return filename


def check_completeness(stream: Stream) -> int:
    datapoints = sum(trace.count() for trace in stream.traces) - 1
    return int((datapoints - 1) * 1000 / stream.traces[0].stats.sampling_rate)


def check_shortest_trace(stream: Stream) -> int:
    sampling_rate = stream.traces[0].stats.sampling_rate
    shortest_trace = min(len(trace) for trace in stream.traces) - 1
    return int(math.ceil(shortest_trace / sampling_rate))


def check_removable_response(
    stream: Stream,
    channel: str,
    network: str,
    station: str,
    count: int,
) -> Check.Result:
    if count > 1:
        try:
            stream.merge(fill_value=0)
        except Exception as message:  # noqa: BLE001
            logger.warning(
                "Channel %s.%s.%s have traces with different sampling rates : %s",
                network,
                station,
                channel,
                message,
            )
            return Check.Result.DECONVOLUTION_FAILS
    trace = stream.traces[0]

    # If instrument is environmental (RWD, etc...), do not try to remove response
    if channel[INSTRUMENT_CODE] in SEISMOMETER_CHANNELS:
        # decimate the trace only for hight frequency channels
        if channel[BAND_CODE] in HIGH_FREQUENCY_CHANNELS:
            decimation_factor = int(trace.stats.sampling_rate / TARGET_SAMPLING_RATE)
            trace.decimate(decimation_factor, no_filter=True)

        try:
            inventory = read_inventory(
                f"{settings.WAVEQC_STATIONXML_STORAGE_PATH}/{network}_{station}.xml",
            )
            trace.remove_response(inventory=inventory)
        except (ValueError, FileNotFoundError, TypeError):
            result = Check.Result.DECONVOLUTION_FAILS
        else:
            result = Check.Result.DECONVOLUTION_PASS
    else:
        result = Check.Result.DECONVOLUTION_PASS
    return result


@shared_task(
    ignore_result=True, autoretry_for=(Exception,), retry_backoff=10, retry_jitter=True
)
def check_item(mseed: str) -> None:
    completeness = count = shortest_trace = 0
    file = Path(mseed)
    nslc, start, _ = file.name.split("__")
    network, station, _, channel = nslc.split(".")
    date = pendulum.parse(start[:8], exact=True)
    try:
        stream = read(file)
    except (TypeError, InternalMSEEDError):
        result = Check.Result.NOT_READABLE
    except FileNotFoundError:
        result = Check.Result.NO_DATA
    else:
        stream._cleanup()  # noqa: SLF001
        completeness = check_completeness(stream)
        count = len(stream.traces)
        shortest_trace = check_shortest_trace(stream)
        result = check_removable_response(stream, channel, network, station, count)
    file.unlink(missing_ok=True)
    Check.objects.store(
        nslc,
        date,  # type: ignore[arg-type]
        result,
        completeness,
        count,
        shortest_trace,
    )


@shared_task(bind=True)
def run_checks(self: Task, checks: list[dict[str, object]]) -> object:
    return self.replace(
        group([chain(download_mseed.si(check), check_item.s()) for check in checks]),
    )


@shared_task(
    ignore_result=True, autoretry_for=(Exception,), retry_backoff=10, retry_jitter=True
)
def remove_stationxmls() -> None:
    basepath = Path(settings.WAVEQC_STATIONXML_STORAGE_PATH)
    for entry in basepath.iterdir():
        entry.unlink()


@shared_task(
    ignore_result=True, autoretry_for=(Exception,), retry_backoff=10, retry_jitter=True
)
def fix_closed_channels_checks() -> None:
    Check.objects.fix_closed_channels()


@shared_task(
    ignore_result=True, autoretry_for=(Exception,), retry_backoff=10, retry_jitter=True
)
def fix_missing_checks() -> None:
    Check.objects.fix_missing()


@shared_task(ignore_result=True)
def retry_failing_checks() -> None:
    date = pendulum.yesterday("utc").to_date_string()
    chain(
        update_inventory.si(date),
        build_station_list.si(date),
        download_stationxmls.s(date),
        get_failed_checks.si(),
        run_checks.s(),
        remove_stationxmls.si(),
        fix_closed_channels_checks.si(),
        fix_missing_checks.si(),
    )()


@shared_task(ignore_results=True)
def launch_checks() -> None:
    date = pendulum.yesterday("utc").to_date_string()
    chain(
        update_inventory.si(date),
        build_station_list.si(date),
        download_stationxmls.s(date),
        build_check_list.si(date),
        run_checks.s(),
        remove_stationxmls.si(),
        fix_closed_channels_checks.si(),
        fix_missing_checks.si(),
    )()
