from django.conf import settings
from django.contrib import admin
from django.db.models import QuerySet
from django.http.request import HttpRequest

from .models import Channel, Check, Network, Operator, Station


class WaveQCModelAdmin(admin.ModelAdmin):  # type: ignore[type-arg]
    """
    Class used to allow changes in developpment and forbid it on production
    """

    def has_add_permission(
        self,
        _request: HttpRequest,
        _obj: object | None = None,
    ) -> bool:
        return settings.DEBUG

    def has_change_permission(
        self,
        _request: HttpRequest,
        _obj: object | None = None,
    ) -> bool:
        return settings.DEBUG

    def has_delete_permission(
        self,
        _request: HttpRequest,
        _obj: object | None = None,
    ) -> bool:
        return settings.DEBUG


@admin.register(Network)
class NetworkAdmin(WaveQCModelAdmin):
    list_display = ("code", "description")


@admin.register(Station)
class StationAdmin(WaveQCModelAdmin):
    list_display = ("code", "network", "start_date", "end_date")
    list_filter = ("network__code", "triggered", "start_date", "end_date")
    ordering = ("network__code", "code")
    search_fields = ("code",)

    def get_queryset(self, request: HttpRequest) -> QuerySet[Station]:
        return super().get_queryset(request).select_related("network")


@admin.register(Channel)
class ChannelAdmin(WaveQCModelAdmin):
    autocomplete_fields = ("station",)

    list_display = ("__str__", "end_date")
    list_filter = ("station__network__code", "end_date")
    ordering = ("station__network__code", "station__code", "location", "code")
    search_fields = ("location", "code", "station__code")

    def get_queryset(self, request: HttpRequest) -> QuerySet[Channel]:
        return super().get_queryset(request).select_related("station__network")


@admin.register(Check)
class CheckAdmin(WaveQCModelAdmin):
    autocomplete_fields = ("channel",)

    date_hierarchy = "date"
    list_display = ("channel", "date")
    list_filter = ("channel__station__network__code", "result", "retries")
    ordering = (
        "channel__station__network__code",
        "channel__station__code",
        "channel__location",
        "channel__code",
    )
    search_fields = (
        "channel__station__network__code",
        "channel__station__code",
        "channel__location",
        "channel__code",
    )


@admin.register(Operator)
class OperatorAdmin(WaveQCModelAdmin):
    autocomplete_fields = ("stations",)

    list_display = ("agency", "website")
    list_filter = ("stations__network",)
    ordering = ("agency",)
    search_fields = ("agency", "stations__code")
