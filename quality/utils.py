import contextlib
from typing import TypedDict

import pendulum
import requests
from django.conf import settings


class Filter(TypedDict):
    year: int | None
    triggered: bool | None
    network: list[str] | None
    station: list[str] | None
    channel: list[str] | None
    operator: list[str] | None


def get_rtserved_stations(network: str) -> list[str]:
    url = f"{settings.RT_SERVER_URL}/streamids"
    with contextlib.suppress(requests.ConnectionError):
        response = requests.get(url, timeout=0.5, params={"level": 2})
        if response.ok:
            return [
                line.split("_")[1]
                for line in response.text.splitlines()
                if line.split("_")[0] == network
            ]
    return []


def get_station_last_rt_data(network: str, station: str) -> dict[str, object]:
    url = f"{settings.RT_SERVER_URL}/streams"
    with contextlib.suppress(requests.ConnectionError):
        response = requests.get(url, timeout=0.5)
        if response.ok:
            return {
                line.split("  ")[0].split("/")[0].replace("_", "."): pendulum.parse(
                    line.split("  ")[-1]
                )
                for line in response.text.splitlines()
                if line.split("_")[0:2] == [network, station]
            }
    return {}
