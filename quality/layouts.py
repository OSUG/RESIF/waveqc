from .models import RESULT_PONDERATION, Check

ZMAX = 355
HEATMAP_LAYOUT_COMPLETENESS = {
    "x": [""],
    "showscale": False,
    "ygap": 1,
}
HEATMAP_LAYOUT_NETWORK = {
    "colorbar": {
        "title_text": "Data Quality and Completeness",
        "title_side": "top",
        "tickvals": [25, 75, 125, 200, 255, 305, 345],
        "ticktext": [
            "Channel closed",
            "No data",
            "Not readable",
            "Deconvolution fails",
            "0%",
            "Deconvolution pass",
            "100%",
        ],
        "lenmode": "pixels",
        "len": 300,
        "yanchor": "top",
        "y": 1,
    },
    "hovertemplate": (
        "<b>%{y}</b><br>"
        "%{x}<br>"
        "Completeness : %{customdata[0]}%<br>"
        "Traces : %{customdata[2]}<br>"
        "Shortest trace : %{customdata[3]} seconds"
        "<extra></extra>"
    ),
    "colorscale": [
        [0, "black"],
        [RESULT_PONDERATION[Check.Result.NO_DATA] / ZMAX - 0.001, "black"],
        [RESULT_PONDERATION[Check.Result.NO_DATA] / ZMAX, "whitesmoke"],
        [RESULT_PONDERATION[Check.Result.NOT_READABLE] / ZMAX - 0.001, "whitesmoke"],
        [RESULT_PONDERATION[Check.Result.NOT_READABLE] / ZMAX, "#a50026"],
        [
            RESULT_PONDERATION[Check.Result.DECONVOLUTION_FAILS] / ZMAX - 0.001,
            "#a50026",
        ],
        [RESULT_PONDERATION[Check.Result.DECONVOLUTION_FAILS] / ZMAX, "#f46d43"],
        [(RESULT_PONDERATION[Check.Result.DECONVOLUTION_PASS] - 5) / ZMAX, "#f46d43"],
        [(RESULT_PONDERATION[Check.Result.DECONVOLUTION_PASS] - 4) / ZMAX, "#e0f3f8"],
        [0.99, "#4575b4"],
        [1, "#313695"],
        # [RESULT_PONDERATION[Check.Result.DECONVOLUTION_PASS/ZMAX, "#d9ef8b"],
        # [0.99, "#1a9850"],
        # [1, "#006837"],
    ],
    "xgap": 1,
    "ygap": 1,
    "zmin": 0,
    "zmax": ZMAX,
}
HEATMAP_LAYOUT_QUALITY = HEATMAP_LAYOUT_NETWORK | {
    "colorbar_len": 0.5,
    "colorbar_lenmode": "fraction",
    "colorbar_y": 1.05,
}
HEATMAP_LAYOUT_STATION = {
    "colorbar": {"title_text": "Number of traces", "len": 0.5, "y": 0.25},
    "colorscale": list(
        reversed(
            [
                [1, "rgb(165,0,38)"],
                [0.9, "rgb(215,48,39)"],
                [0.8, "rgb(244,109,67)"],
                [0.7, "rgb(253,174,97)"],
                [0.6, "rgb(254,224,139)"],
                [0.5, "rgb(255,255,191)"],
                [0.4, "rgb(217,239,139)"],
                [0.3, "rgb(166,217,106)"],
                [0.2, "rgb(102,189,99)"],
                [0.1, "rgb(26,152,80)"],
                [0.001, "rgb(0,104,55)"],
                [0, "whitesmoke"],
            ],
        ),
    ),
    "hovertemplate": ("<b>%{y}</b><br>%{x}<br>Traces : %{z}<extra></extra>"),
    "xgap": 1,
    "ygap": 1,
    "zmin": 0,
}
