from django.urls import path

from .views import (
    CheckFullOverview,
    CheckOverview,
    HealthCheck,
    HomePage,
    NetworkDetail,
    NetworkFullDetail,
    NetworkList,
    NetworkOverview,
    OperatorList,
    StationDetail,
)

app_name = "quality"
urlpatterns = [
    path("", HomePage.as_view(), name="homepage"),
    path("healthcheck/", HealthCheck.as_view(), name="healthcheck"),
    path("checks/", CheckOverview.as_view(), name="check-overview"),
    path("checks/full/", CheckFullOverview.as_view(), name="check-full-overview"),
    path("networks/", NetworkList.as_view(), name="network-list"),
    path("networks/<int:pk>/", NetworkDetail.as_view(), name="network-detail"),
    path("networks/<int:pk>/full/", NetworkFullDetail.as_view(), name="network-full-detail"),  # noqa: E501
    path("networks/<int:pk>/overview/", NetworkOverview.as_view(), name="network-overview"),  # noqa: E501
    path("operators/", OperatorList.as_view(), name="operator-list"),
    path("stations/<int:pk>/", StationDetail.as_view(), name="station-detail"),
]  # fmt: skip
