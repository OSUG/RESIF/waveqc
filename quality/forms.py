from django import forms


class MultiChoiceField(forms.TypedMultipleChoiceField):
    """
    Multichoice field inherits from MultipleChoiceField but
    doesn't check if values are valid against choices
    """

    def valid_value(self, _value: str) -> bool:
        return True


class FilterForm(forms.Form):
    year = forms.IntegerField(required=False)
    triggered = forms.NullBooleanField(required=False)
    network = MultiChoiceField(required=False, coerce=str, empty_value=None)
    station = MultiChoiceField(required=False, coerce=str, empty_value=None)
    channel = MultiChoiceField(required=False, coerce=str, empty_value=None)
    operator = MultiChoiceField(required=False, coerce=str, empty_value=None)
