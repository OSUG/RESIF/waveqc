# ruff: noqa: S101
import pytest
from django.http.request import QueryDict

from quality.models import Check
from quality.templatetags.quality_extras import colorize, getlist


@pytest.fixture
def query_dict() -> QueryDict:
    return QueryDict("a=1&a=2&a=3")


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        (Check.Result.NO_DATA, "danger"),
        (Check.Result.NOT_READABLE, "danger"),
        (Check.Result.DECONVOLUTION_FAILS, "warning"),
        (Check.Result.DECONVOLUTION_PASS, "success"),
        (Check.Result.CHANNEL_CLOSED, "dark"),
    ],
)
def test_cororize(value: int, expected: str) -> str:
    assert colorize(value) == expected


@pytest.mark.parametrize(
    ("key", "expected"),
    [("plop", []), ("a", ["1", "2", "3"])],
)
def test_getlist(key: str, expected: list, query_dict: QueryDict) -> str:
    assert getlist(query_dict, key) == expected
