# ruff: noqa: S101
import pendulum
import pytest
from django.test import RequestFactory
from pytest_django.asserts import assertContains, assertQuerySetEqual

from quality.factories import CheckFactory, OperatorFactory, StationFactory
from quality.models import Check, Station
from quality.views import HealthCheck, Heatmap, HomePage, NetworkOverview


@pytest.fixture
def check() -> Check:
    check = CheckFactory.create(channel__code="HNE")
    OperatorFactory.create(stations=[check.channel.station], agency="OSUG")
    return check


@pytest.fixture
def yesterday() -> pendulum.Date:
    return pendulum.yesterday("utc")


@pytest.fixture
def last_year() -> pendulum.Date:
    return pendulum.yesterday("utc").subtract(years=1)


#####################################################################
### HealthCheck View Tests
#####################################################################
def test_healthcheck_view(rf: RequestFactory) -> None:
    request = rf.get("/healthcheck")
    view = HealthCheck()
    view.setup(request)
    assertContains(view.get(request), "OK")


#####################################################################
### CheckFormMixin Tests
#####################################################################
@pytest.mark.django_db
def test_check_form_mixin_get_context_data(rf: RequestFactory, check: Check) -> None:
    request = rf.get("/")
    # Here we use HomePage view instead of CheckFormMixin becausethe
    # the later one don't have a setup method
    view = HomePage()
    view.setup(request)

    context = view.get_context_data()
    assertQuerySetEqual(context["networks"], [check.channel.station.network])
    assertQuerySetEqual(context["stations"], [check.channel.station])
    assertQuerySetEqual(context["operators"], [check.channel.station.operators.first()])
    assertQuerySetEqual(context["channels"], [check.channel.code[:2]])


#####################################################################
### NetworkOverview View Tests
#####################################################################
@pytest.mark.django_db
def test_network_overview_view(
    rf: RequestFactory, yesterday: pendulum.Date, last_year: pendulum.Date
) -> None:
    triggered = StationFactory.create(
        triggered=True, network__pk=1, start_date=last_year
    )
    continuous = StationFactory.create(
        triggered=False, network=triggered.network, start_date=last_year
    )
    CheckFactory(date=yesterday, channel__station=continuous, channel__end_date=None)
    CheckFactory(date=yesterday, channel__station=triggered, channel__end_date=None)
    stations = Station.objects.overview(1)

    request = rf.get("/networks/1/overview/")
    view = NetworkOverview()
    view.setup(request, pk=1)
    view.object = view.get_object()
    context = view.get_context_data()

    assertQuerySetEqual(context["stations"], stations.filter(triggered=False))
    assertQuerySetEqual(context["triggered_stations"], stations.filter(triggered=True))


#####################################################################
### Heatmap Mixin Tests
#####################################################################
def test_heatmap_get_summary() -> None:
    mixin = Heatmap()
    with pytest.raises(NotImplementedError):
        mixin.get_summary(None)


def test_heatmap_years_available() -> None:
    mixin = Heatmap()
    with pytest.raises(NotImplementedError):
        mixin.years_available(None)
