# ruff: noqa: S101
import datetime
from typing import Any

import pendulum
import pytest
from django.utils import timezone
from pytest_django.asserts import assertQuerySetEqual

from quality.factories import (
    ChannelFactory,
    CheckFactory,
    OperatorFactory,
    StationFactory,
)
from quality.models import (
    CHECK_MAX_RETRIES,
    Channel,
    Check,
    CheckQuerySet,
    Operator,
    Station,
    StationQuerySet,
)

timezone.now()


def date(year: int, month: int, day: int) -> datetime.datetime:
    return datetime.datetime(year, month, day, tzinfo=datetime.UTC)


#####################################################################
### Network QuerySet Tests
#####################################################################


#####################################################################
### Station QuerySet Tests
#####################################################################
@pytest.fixture
def all_stations(today: pendulum.Date) -> list[Station]:
    stations = [
        StationFactory.create(
            triggered=True,
            code="FOLK",
            network__pk=1,
            network__code="FR",
            start_date=today.subtract(years=1),
        ),
        StationFactory.create(
            triggered=False,
            code="ROCK",
            start_date=today.subtract(years=1),
            network__code="GB",
        ),
        StationFactory.create(
            triggered=False,
            code="COUNTRY",
            start_date=date(2022, 5, 6),
            end_date=date(2023, 5, 6),
            network__code="US",
        ),
    ]
    OperatorFactory(agency="OSUG", stations=[stations[0]])
    CheckFactory.create(
        channel__station=stations[0],
        channel__end_date=today.add(years=1),
        date=today.subtract(days=5),
        result=Check.Result.DECONVOLUTION_PASS,
    )
    CheckFactory.create(
        channel__station=stations[0],
        channel__end_date=None,
        date=today.subtract(days=6),
        result=Check.Result.DECONVOLUTION_FAILS,
    )
    CheckFactory.create(
        channel__station=stations[0],
        channel__end_date=today.add(years=1),
        date=today.subtract(days=7),
        result=Check.Result.NOT_READABLE,
    )
    CheckFactory.create(
        channel__station=stations[2],
        channel__end_date=date(2023, 5, 6),
        date=date(2023, 5, 6),
        result=Check.Result.NOT_READABLE,
    )
    return stations


@pytest.fixture
def triggered_stations() -> StationQuerySet:
    return Station.objects.filter(triggered=True)


@pytest.fixture
def continuous_stations() -> StationQuerySet:
    return Station.objects.filter(triggered=False)


@pytest.fixture
def rem_stations() -> StationQuerySet:
    return Station.objects.filter(network__code="REM")


@pytest.fixture
def folk_rock_stations() -> StationQuerySet:
    return Station.objects.filter(code__in=["FOLK", "ROCK"])


@pytest.fixture
def stations_summary(today: pendulum.Date) -> CheckQuerySet:
    return [
        ("COUNTRY", 3, date(2023, 5, 6).date(), 0, 1, 0, 0, "US"),
        ("ROCK", 2, None, None, None, None, None, "GB"),
        ("FOLK", 1, today.subtract(days=7).date(), 0, 1, 0, 0, "FR"),
        ("FOLK", 1, today.subtract(days=6).date(), 0, 2, 0, 0, "FR"),
        ("FOLK", 1, today.subtract(days=5).date(), 0, 3, 0, 0, "FR"),
    ]


@pytest.fixture
def osug_stations() -> StationQuerySet:
    return Station.objects.filter(operators__agency="OSUG")


@pytest.fixture
def current_month_stations() -> StationQuerySet:
    return Station.objects.filter(code="FOLK")


@pytest.fixture
def last_year_stations() -> StationQuerySet:
    return Station.objects.filter(code="COUNTRY")


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("query", "result"),
    [
        (Station.objects.triggered("1"), "triggered_stations"),
        (Station.objects.triggered("0"), "continuous_stations"),
        (Station.objects.triggered(), "all_stations"),
        (Station.objects.stations(["FOLK", "ROCK"]), "folk_rock_stations"),
        (Station.objects.stations(), "all_stations"),
        (Station.objects.networks(["REM"]), "rem_stations"),
        (Station.objects.networks(), "all_stations"),
        (Station.objects.channels(), "all_stations"),
        (Station.objects.channels(["HN"]), "all_stations"),
        (Station.objects.operators(), "all_stations"),
        (Station.objects.operators(["OSUG"]), "osug_stations"),
        (Station.objects.channels().year(), "current_month_stations"),
        (Station.objects.channels().year("2023"), "last_year_stations"),
    ],
)
def test_station_querysets(
    query: Any,
    result: list[Station],
    request: Any,
    all_stations: list[Station],  # noqa:ARG001
) -> None:
    # See the following link to see how to include fixtures in parametrized tests
    # https://engineeringfordatascience.com/posts/pytest_fixtures_with_parameterize/
    result = request.getfixturevalue(result)
    assertQuerySetEqual(query, result, ordered=False)


@pytest.mark.django_db
def test_station_queryset_summary(
    all_stations: list[Station],  # noqa:ARG001
    stations_summary: list,
) -> None:
    assertQuerySetEqual(Station.objects.channels().summary(), stations_summary)


@pytest.mark.django_db
def test_station_queryset_opened() -> None:
    """
    result should exclude station closed before and started after requested date
    """
    station1 = StationFactory.create(start_date=date(2022, 5, 6))
    _station2 = StationFactory.create(start_date=date(2023, 5, 6))
    _station3 = StationFactory.create(
        start_date=date(2022, 5, 6), end_date=date(2022, 6, 7)
    )
    station4 = StationFactory.create(
        start_date=date(2022, 5, 6), end_date=date(2023, 6, 7)
    )
    assertQuerySetEqual(
        Station.objects.opened(day="2023-01-01"), [station1, station4], ordered=False
    )


@pytest.mark.django_db
def test_station_queryset_overview(
    all_stations: list[Station],  # noqa:ARG001
) -> None:
    overview = Station.objects.overview(1)
    expected = Station.objects.get(code="FOLK")
    assertQuerySetEqual(
        overview,
        [
            {
                "code": "FOLK",
                "id": expected.id,
                "result": Check.Result.DECONVOLUTION_FAILS,
            }
        ],
    )


#####################################################################
### Channel QuerySet Tests
#####################################################################
@pytest.mark.django_db
def test_channel_queryset_instruments() -> None:
    """Result should deduplicate instruments"""
    ChannelFactory.create(code="HHE", location="00")
    ChannelFactory.create(code="HHN", location="00")
    ChannelFactory.create(code="HHE", location="01")
    ChannelFactory.create(code="HNE", location="01")
    assertQuerySetEqual(Channel.objects.instruments(), ["HH", "HN"])


@pytest.mark.django_db
def test_channel_queryset_checks_exist() -> None:
    channel1 = ChannelFactory.create()
    ChannelFactory.create()
    CheckFactory.create(channel=channel1)
    assertQuerySetEqual(Channel.objects.checks_exist(), [channel1])


@pytest.mark.django_db
def test_channel_queryset_opened() -> None:
    """
    result should exclude :
    - channels whose station is created after build_day
    - channels closed before build_day
    """
    station1 = StationFactory.create(start_date=date(2022, 5, 6))
    station2 = StationFactory.create(start_date=date(2023, 5, 6))
    channel1 = ChannelFactory.create(station=station1, end_date=None)
    _channel2 = ChannelFactory.create(station=station2, end_date=None)
    _channel3 = ChannelFactory.create(station=station1, end_date=date(2022, 5, 10))
    channel4 = ChannelFactory.create(station=station1, end_date=date(2023, 5, 10))
    assertQuerySetEqual(
        Channel.objects.opened(day="2023-01-01"), [channel1, channel4], ordered=False
    )


#####################################################################
### Check QuerySet Tests
#####################################################################
@pytest.fixture
def all_checks(today: pendulum.Date) -> list[Check]:
    checks = [
        CheckFactory.create(
            channel__station__network__code="REM",
            channel__station__code="FOLK",
            channel__station__triggered=True,
            date=today.subtract(days=15),
            channel__station__start_date=today.subtract(years=1),
            channel__code="HHE",
        ),
        CheckFactory.create(
            channel__station__network__code="REM",
            channel__station__code="FOLK",
            channel__station__triggered=True,
            date=today.subtract(days=15),
            channel__end_date=today.subtract(days=5),
            channel__station__start_date=today.subtract(years=1),
            channel__code="HHZ",
        ),
        CheckFactory.create(
            channel__station__network__code="IAM",
            channel__station__code="ROCK",
            channel__station__triggered=False,
            date=pendulum.datetime(2022, 1, 1),
            channel__end_date=None,
            channel__station__start_date=today.subtract(years=3),
            channel__code="HNE",
        ),
    ]
    OperatorFactory.create(stations=[Station.objects.get(code="ROCK")], agency="BRGM")
    return checks


@pytest.fixture
def iam_network_checks() -> CheckQuerySet:
    return Check.objects.filter(channel__station__network__code="IAM")


@pytest.fixture
def folk_station_checks() -> CheckQuerySet:
    return Check.objects.filter(channel__station__code="FOLK")


@pytest.fixture
def triggered_station_checks() -> CheckQuerySet:
    return Check.objects.filter(channel__station__triggered=True)


@pytest.fixture
def continuous_station_checks() -> CheckQuerySet:
    return Check.objects.filter(channel__station__triggered=False)


@pytest.fixture
def current_month_checks() -> CheckQuerySet:
    return Check.objects.filter(channel__station__code="FOLK")


@pytest.fixture
def checks_from_2022() -> CheckQuerySet:
    return Check.objects.filter(channel__station__code="ROCK")


@pytest.fixture
def hne_checks() -> CheckQuerySet:
    return Check.objects.filter(channel__code="HNE")


@pytest.fixture
def brgm_checks() -> CheckQuerySet:
    return Check.objects.filter(channel__station__operators__agency="BRGM")


@pytest.fixture
def checks_summary() -> CheckQuerySet:
    return [
        (
            check.channel.id,
            check.channel.code,
            check.date,
            check.completeness,
            check.result,
            check.trace_count,
            check.shortest_trace,
            check.channel.location,
            check.channel.station.code,
            check.channel.station.network.code,
        )
        for check in Check.objects.all()
        .select_related("channel__station__network")
        .order_by(
            "-channel__station__network__code",
            "-channel__station__code",
            "-channel__location",
            "-channel__code",
            "date",
        )
    ]


@pytest.mark.django_db
@pytest.mark.parametrize(
    ("query", "result"),
    [
        (Check.objects.networks(), "all_checks"),
        (Check.objects.networks(["IAM"]), "iam_network_checks"),
        (Check.objects.stations(), "all_checks"),
        (Check.objects.stations(["FOLK"]), "folk_station_checks"),
        (Check.objects.triggered("1"), "triggered_station_checks"),
        (Check.objects.triggered("0"), "continuous_station_checks"),
        (Check.objects.triggered(), "all_checks"),
        (Check.objects.year(), "current_month_checks"),
        (Check.objects.year("2022"), "checks_from_2022"),
        (Check.objects.channels(), "all_checks"),
        (Check.objects.channels(["HNE"]), "hne_checks"),
        (Check.objects.operators(), "all_checks"),
        (Check.objects.operators(["BRGM"]), "brgm_checks"),
        (Check.objects.summary(), "checks_summary"),
    ],
)
def test_check_querysets(
    query: Any,
    result: list[Check],
    request: Any,
    all_checks: list[Check],  # noqa:ARG001
) -> None:
    result = request.getfixturevalue(result)
    assertQuerySetEqual(query, result, ordered=False)


@pytest.mark.django_db
def test_check_queryset_failed() -> None:
    """
    result should only include :
    - checks that fails (result lower than deconvolution_pass)
    - checks that have not reached maximum retries
    """
    _check1 = CheckFactory.create(
        retries=CHECK_MAX_RETRIES, result=Check.Result.NOT_READABLE
    )
    _check2 = CheckFactory.create(
        retries=CHECK_MAX_RETRIES,
        result=Check.Result.DECONVOLUTION_PASS,
    )
    check3 = CheckFactory.create(retries=1, result=Check.Result.NOT_READABLE)
    _check4 = CheckFactory.create(retries=1, result=Check.Result.DECONVOLUTION_PASS)
    assertQuerySetEqual(Check.objects.failed(), [check3])


#####################################################################
### Operator QuerySet Tests
#####################################################################
@pytest.mark.django_db
def test_operator_queryset_acronyms_only() -> None:
    OperatorFactory.create(agency="Observatoire de la Côte d'Azur (OCA)")
    OperatorFactory.create(agency="OSUG")
    assert Operator.objects.acronyms_only() == ["OCA", "OSUG"]


@pytest.mark.django_db
def test_operator_queryset_checks_exist() -> None:
    CheckFactory.create(channel__station__code="ROCK")
    OperatorFactory.create(agency="Observatoire de la Côte d'Azur (OCA)")
    OperatorFactory.create(agency="OSUG", stations=[Station.objects.get(code="ROCK")])
    assertQuerySetEqual(
        Operator.objects.checks_exist(), Operator.objects.filter(agency="OSUG")
    )
