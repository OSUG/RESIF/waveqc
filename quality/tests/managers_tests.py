# ruff: noqa: S101
import pendulum
import pytest
from faker import Faker
from obspy import UTCDateTime
from obspy.core.inventory import Inventory
from obspy.core.inventory.channel import Channel as ObspyChannel
from obspy.core.inventory.network import Network as ObspyNetwork
from obspy.core.inventory.station import Station as ObspyStation
from obspy.core.inventory.util import Operator as ObspyOperator
from pytest_django.asserts import assertQuerySetEqual

from quality.factories import ChannelFactory
from quality.models import Channel, Check, Network, Operator, Station

fake = Faker()


@pytest.fixture
def inventory() -> Inventory:
    operator = ObspyOperator(agency=fake.company())
    channels = [
        ObspyChannel(
            code="HHE",
            location_code="00",
            latitude=fake.latitude(),
            longitude=fake.longitude(),
            elevation=fake.random_digit_above_two(),
            depth=fake.random_digit_above_two(),
            start_date=UTCDateTime(fake.date_this_year(before_today=True)),
            end_date=UTCDateTime(fake.date_this_year(before_today=True)),
        ),
        ObspyChannel(
            code="HHE",
            location_code="00",
            latitude=fake.latitude(),
            longitude=fake.longitude(),
            elevation=fake.random_digit_above_two(),
            depth=fake.random_digit_above_two(),
            start_date=UTCDateTime(fake.date_this_year(before_today=False)),
            end_date=UTCDateTime(fake.date_this_year(before_today=False)),
        ),
    ]
    station = ObspyStation(
        code="FOLK",
        latitude=fake.latitude(),
        longitude=fake.longitude(),
        elevation=fake.random_digit_above_two(),
        start_date=UTCDateTime(fake.date()),
        end_date=UTCDateTime(fake.date()),
        channels=channels,
        operators=[operator],
    )
    network = ObspyNetwork(code="FR", description=fake.text(), stations=[station])
    return Inventory(networks=[network])


#####################################################################
### Network Manager Tests
#####################################################################
@pytest.mark.django_db
def test_network_manager_populate(inventory: Inventory) -> None:
    # Creation test
    assertQuerySetEqual(Network.objects.all(), [])
    Network.objects.populate(inventory)
    network = Network.objects.first()
    assert network.code == inventory.networks[0].code
    assert network.description == inventory.networks[0].description

    # Update test :
    # we modify description in database. Populate networks again and seeif
    # if update has been done correctly without creating a new network
    network.description = "changed description"
    network.save()
    Network.objects.populate(inventory)
    network = Network.objects.first()
    assertQuerySetEqual(Network.objects.all(), [network])
    assert network.description == inventory.networks[0].description


#####################################################################
### Station Manager Tests
#####################################################################
@pytest.mark.django_db
def test_station_manager_populate(inventory: Inventory, today: pendulum.Date) -> None:
    # Creation test
    assertQuerySetEqual(Station.objects.all(), [])
    Network.objects.populate(inventory)
    Station.objects.populate(inventory)
    station = Station.objects.first()
    assert station.code == inventory.networks[0].stations[0].code
    assert station.start_date == inventory.networks[0].stations[0].start_date
    assert station.end_date == inventory.networks[0].stations[0].end_date

    # Update test :
    # we modify end_date in database. Populate stations again and see
    # if update has been done correctly without creating a new station
    station.end_date = today
    station.save()
    Station.objects.populate(inventory)
    station = Station.objects.first()
    assertQuerySetEqual(Station.objects.all(), [station])
    assert station.end_date == inventory.networks[0].stations[0].end_date


#####################################################################
### Channel Manager Tests
#####################################################################
@pytest.mark.django_db
def test_channel_manager_populate(inventory: Inventory, today: pendulum.Date) -> None:
    # Creation test
    assertQuerySetEqual(Channel.objects.all(), [])
    Network.objects.populate(inventory)
    Station.objects.populate(inventory)
    Channel.objects.populate(inventory)
    channel = Channel.objects.first()
    # Only the latest Epoch is kept
    inv_channel = inventory.networks[0].stations[0].channels[1]
    assert channel.code == inv_channel.code
    assert channel.location == inv_channel.location_code
    assert channel.end_date == inv_channel.end_date
    assertQuerySetEqual(Channel.objects.all(), [channel])

    # Update test :
    # we modify end_date in database. Populate channels again and see
    # if update has been done correctly without creating a new channel
    channel.end_date = today
    channel.save()
    Channel.objects.populate(inventory)
    channel = Channel.objects.first()
    # Only the latest Epoch is kept
    assertQuerySetEqual(Channel.objects.all(), [channel])
    assert channel.end_date == inv_channel.end_date


#####################################################################
### Check Manager Tests
#####################################################################
@pytest.mark.django_db
def test_check_manager_store(today: pendulum.Date) -> None:
    channel = ChannelFactory.create()
    nslc = str(channel)
    Check.objects.store(nslc, today)
    assert channel.check_set.all().count() == 1


#####################################################################
### Operator Manager Tests
#####################################################################
@pytest.mark.django_db
def test_operator_manager_populate(inventory: Inventory) -> None:
    # Creation test
    assertQuerySetEqual(Operator.objects.all(), [])
    Network.objects.populate(inventory)
    Station.objects.populate(inventory)
    Operator.objects.populate(inventory)
    operator = Operator.objects.first()
    inv_operator = inventory.networks[0].stations[0].operators[0]
    assert operator.agency == inv_operator.agency


@pytest.mark.django_db
def test_operator_manager_link_to_stations(inventory: Inventory) -> None:
    Network.objects.populate(inventory)
    Station.objects.populate(inventory)
    Operator.objects.populate(inventory)
    assertQuerySetEqual(Station.objects.first().operators.all(), [])
    Operator.objects.link_to_stations(inventory)
    assertQuerySetEqual(
        Station.objects.first().operators.all(), [Operator.objects.first()]
    )


@pytest.mark.django_db
def test_operator_manager_purge_obsoletes(inventory: Inventory) -> None:
    Network.objects.populate(inventory)
    Station.objects.populate(inventory)
    Operator.objects.populate(inventory)
    assert Operator.objects.all().count() == 1
    Operator.objects.purge_obsoletes()
    assert Operator.objects.all().count() == 0
