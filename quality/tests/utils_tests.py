# ruff: noqa: S101
import pendulum
import pytest
from django.conf import settings

from quality.utils import get_rtserved_stations, get_station_last_rt_data


@pytest.mark.parametrize(
    ("response", "status", "expected"),
    [
        ("", 200, []),
        ("FR_NIMR\nRA_CGBP\nCL_AGEO", 200, ["NIMR"]),
        ("", 404, []),
    ],
)
def test_get_rtserved_stations(
    requests_mock: object, response: str, status: int, expected: list[str]
) -> None:
    requests_mock.get(
        f"{settings.RT_SERVER_URL}/streamids?level=2", text=response, status_code=status
    )
    assert get_rtserved_stations("FR") == expected


@pytest.mark.parametrize(
    ("response", "status", "expected"),
    [
        ("", 200, {}),
        (
            (
                "RA_LIQF_00_HNZ/MSEED  2024-06-26T...  2024-06-26T14:14:39.114473Z\n"
                "FR_SPIF_00_BHE/MSEED  2024-06-26T...  2024-06-26T14:14:33.383999Z\n"
            ),
            200,
            {"FR.SPIF.00.BHE": pendulum.parse("2024-06-26T14:14:33.383999Z")},
        ),
        ("", 404, {}),
    ],
)
def test_get_station_last_rt_data(
    requests_mock: object, response: str, status: int, expected: list[str]
) -> None:
    requests_mock.get(
        f"{settings.RT_SERVER_URL}/streams", text=response, status_code=status
    )
    assert get_station_last_rt_data("FR", "SPIF") == expected
