import pendulum
import pytest


@pytest.fixture
def today() -> pendulum.Date:
    return pendulum.today("utc")
