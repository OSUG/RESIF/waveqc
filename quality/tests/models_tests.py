# ruff: noqa: S101
from django.urls import reverse

from quality.factories import (
    ChannelFactory,
    CheckFactory,
    NetworkFactory,
    OperatorFactory,
    StationFactory,
)


#####################################################################
### Network model Tests
#####################################################################
### Methods
def test_network_str() -> None:
    network = NetworkFactory.build()
    assert str(network) == network.code


#####################################################################
### Station model Tests
#####################################################################
def test_station_get_absolute_url() -> None:
    station = StationFactory.build(pk=1)
    assert station.get_absolute_url() == reverse(
        "quality:station-detail", kwargs={"pk": station.pk}
    )


#####################################################################
### Channel model Tests
#####################################################################
def test_channel_str() -> None:
    channel = ChannelFactory.build(code="HHE", location="00")
    assert str(channel) == f"{channel.station}.{channel.location}.{channel.code}"


#####################################################################
### Check model Tests
#####################################################################
def test_check_str() -> None:
    check = CheckFactory.build()
    assert str(check) == f"{check.channel} - {check.date}"


#####################################################################
### Operator model Tests
#####################################################################
def test_operator_str() -> None:
    operator = OperatorFactory.build()
    assert str(operator) == operator.agency
