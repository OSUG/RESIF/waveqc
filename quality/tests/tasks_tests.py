# ruff: noqa: S101
import datetime
import logging
from typing import Any
from zoneinfo import ZoneInfo

import pytest
from obspy.clients.fdsn import Client
from obspy.clients.fdsn.client import (
    FDSNBadRequestException,
    FDSNException,
    FDSNNoDataException,
)
from pytest_mock import MockerFixture

from quality.factories import ChannelFactory, CheckFactory, StationFactory
from quality.models import Check
from quality.tasks import (
    build_check_list,
    build_station_list,
    download_mseed,
    download_stationxml,
    get_failed_checks,
    get_obspy_client,
)


@pytest.fixture
def celery_settings(settings: Any) -> Any:
    settings.CELERY_BROKER_URL = "memory://"
    settings.CELERY_TASKS_ALWAYS_EAGER = True
    settings.CELERY_RESULT_BACKEND = "cache+memory://"
    return settings


@pytest.fixture
def check() -> dict[str, str]:
    return {
        "channel": "HHN",
        "location": "03",
        "station": "ROCK",
        "network": "FR",
        "date": "2023-01-01",
    }


@pytest.fixture
def mseed_filename(settings: Any, check: dict[str, str]) -> str:
    return (
        f"{settings.WAVEQC_MSEED_STORAGE_PATH}/"
        f"{check['network']}."
        f"{check['station']}."
        f"{check['location']}."
        f"{check['channel']}__"
        f"20230101T000000Z__"
        f"20230102T000000Z"
        ".mseed"
    )


def date(year: int, month: int, day: int) -> datetime.datetime:
    return datetime.datetime(year, month, day, tzinfo=ZoneInfo("UTC"))


def test_get_obspy_client(mocker: MockerFixture) -> None:
    # Here we mock Client to avoid an unnecessary request to resif webservice
    mocker.patch.object(Client, "_discover_services", return_value=None)
    client = get_obspy_client()
    assert client.base_url == "http://ws.resif.fr"
    assert "WaveQ" in client.request_headers["User-Agent"]


@pytest.mark.django_db
def test_build_station_list(celery_settings: Any) -> None:  # noqa:ARG001
    build_day = "2023-01-01"
    station = StationFactory.create(start_date=date(2022, 5, 6))
    task = build_station_list.s(build_day).apply()
    assert task.result == [
        f"{station.network.code}_{station.code}" for station in [station]
    ]


@pytest.mark.django_db
def test_build_check_list(celery_settings: Any) -> None:  # noqa:ARG001
    build_day = "2023-01-01"
    station = StationFactory.create(start_date=date(2022, 5, 6))
    channel = ChannelFactory.create(station=station, end_date=None)

    task = build_check_list.s(build_day).apply()
    assert task.result == [
        {
            "channel": channel.code,
            "location": channel.location,
            "station": channel.station.code,
            "network": channel.station.network.code,
            "date": build_day,
        }
        for channel in [channel]
    ]


@pytest.mark.django_db
def test_get_failed_checks(celery_settings: Any) -> None:  # noqa:ARG001
    check = CheckFactory.create(retries=1, result=Check.Result.NOT_READABLE)

    task = get_failed_checks.s().apply()
    assert task.result == [
        {
            "channel": check.channel.code,
            "location": check.channel.location,
            "station": check.channel.station.code,
            "network": check.channel.station.network.code,
            "date": datetime.date.fromisoformat(check.date),
        }
        for check in [check]
    ]


@pytest.mark.parametrize(
    ("side_effect", "logger"),
    [
        (None, []),
        (FDSNNoDataException(""), []),
        (
            FDSNBadRequestException(""),
            [
                (
                    "quality.tasks",
                    logging.WARNING,
                    "Bad request : HHN.03.ROCK.FR.2023-01-01",
                )
            ],
        ),
        (
            FDSNException(""),
            [
                (
                    "quality.tasks",
                    logging.WARNING,
                    "Web Service is not responding : HHN.03.ROCK.FR.2023-01-01",
                )
            ],
        ),
    ],
)
def test_download_mseed(  # noqa:PLR0913
    mocker: MockerFixture,
    check: dict[str, str],
    mseed_filename: str,
    side_effect: Exception,
    caplog: pytest.LogCaptureFixture,
    logger: list[str, int, str],
) -> str:
    # Here we mock Client to avoid an unnecessary request to resif webservice
    mocker.patch.object(Client, "_discover_services", return_value=None)
    mocker.patch.object(Client, "get_waveforms", side_effect=side_effect)
    filename = download_mseed(check)
    assert caplog.record_tuples == logger
    assert filename == mseed_filename


@pytest.mark.parametrize(
    ("side_effect", "logger"),
    [
        (None, []),
        (FDSNNoDataException(""), []),
        (
            FDSNBadRequestException(""),
            [("quality.tasks", logging.WARNING, "Bad request : SALSA")],
        ),
        (
            FDSNException(""),
            [
                (
                    "quality.tasks",
                    logging.WARNING,
                    "Web Service is not responding : SALSA",
                )
            ],
        ),
        (
            AttributeError(""),
            [
                (
                    "quality.tasks",
                    logging.WARNING,
                    "Web Service is not responding : SALSA",
                )
            ],
        ),
    ],
)
def test_download_stationxml(
    mocker: MockerFixture,
    side_effect: Exception,
    caplog: pytest.LogCaptureFixture,
    logger: list[str, int, str],
) -> str:
    # Here we mock Client to avoid an unnecessary request to resif webservice
    mocker.patch.object(Client, "_discover_services", return_value=None)
    mocker.patch.object(Client, "get_stations", side_effect=side_effect)
    download_stationxml(date="2023-01-01", station="FR_SALSA")
    assert caplog.record_tuples == logger
