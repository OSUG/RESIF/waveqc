# ruff: noqa: S101

from quality.forms import MultiChoiceField


def test_mutlichoicefield_valid_value() -> None:
    field = MultiChoiceField()
    assert field.valid_value("plop") is True
