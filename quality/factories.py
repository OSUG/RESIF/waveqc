from string import ascii_uppercase
from typing import Any

import factory

from .models import Channel, Check, Network, Operator, Station


class OperatorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Operator

    agency = factory.Faker("bs")
    website = factory.Faker("url")

    @factory.post_generation
    def stations(self, create: Any, extracted: Any, **__kwargs: Any) -> None:
        if not create or not extracted:
            # Simple build, or nothing to add, do nothing.
            return

        # Add the iterable of stations using bulk addition
        self.stations.add(*extracted)


class NetworkFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Network
        django_get_or_create = ("code",)

    code = factory.Faker("lexify", letters=ascii_uppercase, text="??")
    description = factory.Faker("text")


class StationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Station
        django_get_or_create = ("code",)

    code = factory.Faker("lexify", letters=ascii_uppercase, text="????")
    network = factory.SubFactory(NetworkFactory)


class ChannelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Channel

    code = factory.Faker("lexify", letters=ascii_uppercase, text="???")
    location = factory.Faker("numerify", text="##")
    station = factory.SubFactory(StationFactory)


class CheckFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Check

    date = factory.Faker("date")
    channel = factory.SubFactory(ChannelFactory)
