from itertools import groupby
from typing import Any

import numpy as np
import numpy.typing as npt
import plotly.graph_objects as go
from django.core.exceptions import BadRequest
from django.db.models import Max, Min
from django.http import HttpRequest, HttpResponse
from django.urls import reverse
from django.views import View
from django.views.generic import DetailView, ListView, TemplateView
from django.views.generic.base import ContextMixin
from pendulum.date import Date
from plotly.subplots import make_subplots

from quality.forms import FilterForm

from .layouts import (
    HEATMAP_LAYOUT_COMPLETENESS,
    HEATMAP_LAYOUT_NETWORK,
    HEATMAP_LAYOUT_QUALITY,
    HEATMAP_LAYOUT_STATION,
)
from .models import (
    RESULT_PONDERATION,
    Channel,
    Check,
    Network,
    Operator,
    Station,
)
from .utils import Filter, get_rtserved_stations, get_station_last_rt_data


class HealthCheck(View):
    def get(self, _request: HttpRequest) -> HttpResponse:
        return HttpResponse("OK")


class CheckFormMixin(ContextMixin):
    def get_context_data(self, **kwargs: object) -> dict[str, object]:
        context = super().get_context_data(**kwargs)
        extra = {
            "networks": Network.objects.all(),
            "stations": Station.objects.all().order_by("code"),
            "operators": Operator.objects.checks_exist()
            .only("agency")
            .order_by("agency"),
            "channels": Channel.objects.checks_exist().instruments(),
        }
        context.update(extra)
        return context


class HomePage(CheckFormMixin, TemplateView):
    template_name = "quality/homepage.html"


class OperatorList(ListView[Operator]):
    model = Operator
    ordering = ["agency"]


class NetworkList(ListView[Network]):
    model = Network


class NetworkOverview(DetailView[Network]):
    model = Network
    template_name = "quality/network_overview.html"

    def get_context_data(self, **kwargs: object) -> dict[str, object]:
        context = super().get_context_data(**kwargs)
        query = Station.objects.overview(network_id=self.object.pk)
        context.update(
            {
                "stations": query.filter(triggered=False),
                "triggered_stations": query.filter(triggered=True),
                "rtserved": get_rtserved_stations(network=self.object.code),
            }
        )
        return context


class Heatmap(ContextMixin):
    @staticmethod
    def completeness_trace(mean: npt.NDArray[np.int8]) -> go.Heatmap:
        return go.Heatmap(
            y=[
                (f"<span id='{index}'>{completeness}%</span>")
                for index, completeness in enumerate(mean)
            ],
            z=np.expand_dims(mean, axis=1),
            **HEATMAP_LAYOUT_COMPLETENESS,
        )

    def figure(
        self,
        results: npt.NDArray[np.int8],
        items: list[str],
        dates: list[Date],
    ) -> go.Figure:
        mean = np.round(np.mean(results[:, :, 0], axis=1), 2)
        # Height = items * 15 px + padding
        minimum_height = 400
        layout_height = max(minimum_height, len(items) * 15 + 180)

        figure = make_subplots(specs=[[{"secondary_y": True}]])
        figure.add_trace(
            go.Heatmap(
                x=dates,
                y=items,
                z=results[:, :, 0] + results[:, :, 1],
                customdata=results,
                **HEATMAP_LAYOUT_NETWORK,
            ),
            secondary_y=False,
        )
        figure.add_trace(self.completeness_trace(mean), secondary_y=True)
        figure.update_layout(
            xaxis_side="top",
            height=layout_height,
        )
        return figure.to_html(full_html=False, include_plotlyjs=False)

    def get_summary(self, _filters: Filter) -> object:
        msg = "Subclass must implement abstract method"
        raise NotImplementedError(msg)

    @staticmethod
    def build_x_values(query: Any) -> list[Date]:
        return sorted({key for key, _ in groupby(query, lambda x: x[2])})

    def build_y_values(self, query: Any) -> list[str]:
        return [
            "<a href='{}?{}' target='_self'>{}.{}</a>".format(
                reverse("quality:station-detail", kwargs={"pk": pk}),
                self.request.GET.urlencode(),  # type: ignore[attr-defined]
                network,
                station,
            )
            for (pk, station, network), _ in groupby(
                query,
                lambda x: (x[1], x[0], x[7]),
            )
        ]

    @staticmethod
    def build_z_values(query: Any) -> npt.NDArray[np.int8]:
        # build z values (check results)
        # Warning : Here, we make the assumption that there is a check
        #           for channel each day
        time_to_percent_ratio = 100 / (86400 * 1000)
        return np.array(
            [
                [
                    [
                        check[3] * time_to_percent_ratio,
                        RESULT_PONDERATION[check[4]],
                        check[5],
                        check[6],
                    ]
                    for check in checks
                ]
                for _, checks in groupby(query, lambda x: x[0])
            ],
        )

    def years_available(self, _filters: Filter) -> list[int]:
        msg = "Subclass must implement abstract method"
        raise NotImplementedError(msg)

    def get_context_data(self, **kwargs: object) -> dict[str, object]:
        context = super().get_context_data(**kwargs)
        form = FilterForm(self.request.GET)  # type: ignore[attr-defined]
        if not form.is_valid():
            raise BadRequest(form.errors.as_data())
        filters: Filter = form.cleaned_data  # type: ignore[assignment]
        query = self.get_summary(filters)
        if query:
            dates = self.build_x_values(query)
            items = self.build_y_values(query)
            results = self.build_z_values(query)
            years_available = self.years_available(filters)

            context.update(
                {
                    "figure": self.figure(results=results, items=items, dates=dates),
                    "years_available": years_available,
                },
            )
        return context


class FullHeatmap(Heatmap):
    @staticmethod
    def build_y_values(query: Any) -> list[str]:
        return [
            key
            for key, _ in groupby(
                query,
                lambda x: f"{x[9]}.{x[8]}.{x[7]}.{x[1]}",
            )
        ]


class NetworkDetail(Heatmap, DetailView[Network]):
    model = Network

    def years_available(self, _filters: Filter) -> list[int]:
        dates = Check.objects.filter(
            channel__station__network_id=self.object.id,
        ).aggregate(min=Min("date"), max=Max("date"))
        return list(range(dates["min"].year, dates["max"].year + 1))

    def get_summary(self, filters: Filter) -> object:
        return (
            Station.objects.filter(network_id=self.object.pk)
            .channels(filters["channel"])
            .year(filters["year"])
            .triggered(filters["triggered"])
            .summary()
        )


class NetworkFullDetail(FullHeatmap, NetworkDetail):
    def get_summary(self, filters: Filter) -> object:
        return (
            Check.objects.filter(channel__station__network_id=self.object.pk)
            .year(filters["year"])
            .triggered(filters["triggered"])
            .summary()
        )


class CheckOverview(Heatmap, CheckFormMixin, TemplateView):
    template_name = "quality/check_overview.html"

    def years_available(self, filters: Filter) -> list[int]:
        dates = (
            Check.objects.networks(filters["network"])
            .stations(filters["station"])
            .channels(filters["channel"])
            .operators(filters["operator"])
            .triggered(filters["triggered"])
            .aggregate(min=Min("date"), max=Max("date"))
        )
        return list(range(dates["min"].year, dates["max"].year + 1))

    def get_summary(self, filters: Filter) -> object:
        return (
            Station.objects.networks(filters["network"])
            .stations(filters["station"])
            .channels(filters["channel"])
            .operators(filters["operator"])
            .year(filters["year"])
            .triggered(filters["triggered"])
            .summary()
        )


class CheckFullOverview(FullHeatmap, CheckOverview):
    def get_summary(self, filters: Filter) -> object:
        return (
            Check.objects.networks(filters["network"])
            .stations(filters["station"])
            .channels(filters["channel"])
            .operators(filters["operator"])
            .year(filters["year"])
            .triggered(filters["triggered"])
            .summary()
        )


class StationDetail(FullHeatmap, DetailView[Station]):
    model = Station
    queryset = Station.objects.all().select_related("network")

    def figure(
        self,
        results: npt.NDArray[np.int8],
        items: list[str],
        dates: list[Date],
    ) -> go.Figure:
        minimum_height = 450
        layout_height = max(minimum_height, len(items) * 60 + 200)

        mean = np.round(np.mean(results[:, :, 0], axis=1), 2)
        figure = make_subplots(
            rows=2,
            cols=1,
            shared_xaxes=True,
            specs=[[{"secondary_y": True}], [{"secondary_y": False}]],
        )
        figure.add_trace(
            go.Heatmap(
                x=dates,
                y=items,
                z=results[:, :, 0] + results[:, :, 1],
                customdata=results,
                **HEATMAP_LAYOUT_QUALITY,
            ),
            row=1,
            col=1,
            secondary_y=False,
        )
        figure.add_trace(self.completeness_trace(mean), secondary_y=True)
        # We need to know the maximum number of traces for all checks
        zmax = max(np.max(results[:, :, 2]), 4)
        HEATMAP_LAYOUT_STATION.update({"zmax": zmax})
        figure.add_trace(
            go.Heatmap(x=dates, y=items, z=results[:, :, 2], **HEATMAP_LAYOUT_STATION),
            row=2,
            col=1,
            secondary_y=False,
        )
        figure.update_xaxes(side="top")
        figure.update_layout(height=layout_height)
        return figure.to_html(full_html=False, include_plotlyjs=False)

    def years_available(self, _filters: Filter) -> list[int]:
        dates = Check.objects.filter(
            channel__station_id=self.object.id,
        ).aggregate(min=Min("date"), max=Max("date"))
        return list(range(dates["min"].year, dates["max"].year + 1))

    def get_summary(self, filters: Filter) -> object:
        return (
            Check.objects.filter(channel__station_id=self.object.pk)
            .year(filters["year"])
            .summary()
        )

    def get_context_data(self, **kwargs: object) -> dict[str, object]:
        context = super().get_context_data(**kwargs)
        context["last_data"] = get_station_last_rt_data(
            network=self.object.network.code, station=self.object.code
        )
        return context
