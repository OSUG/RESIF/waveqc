import datetime
from itertools import groupby
from typing import Any

import pendulum
from django.db import models
from django.db.models import Exists, F, FilteredRelation, Max, Min, OuterRef, Q
from django.urls import reverse
from obspy.core.inventory import Inventory
from pendulum.date import Date
from pendulum.datetime import DateTime


class ChannelManager(models.Manager["Channel"]):
    def populate(self, inventory: Inventory) -> None:
        stations = {
            network.code: {
                station.code: station.pk for station in network.station_set.all()
            }
            for network in Network.objects.prefetch_related("station_set").only(
                "code", "station__code", "station__id"
            )
        }
        channels = [
            {
                "code": channel.code,
                "location": channel.location_code,
                "end_date": None
                if channel.end_date.datetime.replace(tzinfo=datetime.UTC)
                > pendulum.now("utc")
                else channel.end_date.datetime.replace(tzinfo=datetime.UTC),
                "raw_end_date": channel.end_date.datetime,
                "station_id": stations[network.code][station.code],
            }
            for network in inventory.networks
            for station in network.stations
            for channel in station.channels
        ]

        channels.sort(
            reverse=True,
            key=lambda x: (
                x["station_id"],
                x["code"],
                x["location"],
                x["raw_end_date"],
            ),
        )  # Channels are now (reverse) sorted by nslc's and end_date
        # Here, we filter only last epochs for each channel
        last_channels = [
            next(grouped)  # we take only the first element (latest end_date)
            for _, grouped in groupby(
                channels,
                lambda x: (x["station_id"], x["code"], x["location"]),
            )
        ]
        for channel in last_channels:
            del channel["raw_end_date"]
        to_insert = [Channel(**channel) for channel in last_channels]
        Channel.objects.bulk_create(
            to_insert,
            update_conflicts=True,
            update_fields=["code", "location", "end_date", "station_id"],
            unique_fields=["code", "station", "location"],
        )


class ChannelQuerySet(models.QuerySet["Channel"]):
    def checks_exist(self) -> "ChannelQuerySet":
        return self.filter(Exists(Check.objects.filter(channel__pk=OuterRef("pk"))))

    def instruments(self) -> list[str]:
        return sorted({channel["code"][:2] for channel in self.values("code")})

    def opened(self, day: DateTime) -> "ChannelQuerySet":
        return self.filter(
            Q(station__start_date__lte=day),
            Q(end_date__isnull=True) | Q(end_date__gt=day),
        )


class Channel(models.Model):
    code = models.CharField(max_length=3)
    location = models.CharField(max_length=8)
    station = models.ForeignKey("Station", models.CASCADE)
    end_date = models.DateTimeField(blank=True, null=True)
    objects = ChannelManager.from_queryset(ChannelQuerySet)()

    class Meta:
        unique_together = (("code", "station", "location"),)

    def __str__(self) -> str:
        return f"{self.station}.{self.location}.{self.code}"


class CheckManager(models.Manager["Check"]):
    def store(  # noqa: PLR0913
        self,
        nslc: str,
        date: Date,
        result: int = 0,
        completeness: int = 0,
        trace_count: int = 0,
        shortest_trace: int = 0,
    ) -> None:
        _, station, location, channel = nslc.split(".")
        channel_checked = Channel.objects.only("pk").get(
            code=channel,
            location=location,
            station__code=station,
        )
        self.update_or_create(
            channel_id=channel_checked.pk,
            date=date,
            defaults={
                "retries": F("retries") + 1,
                "result": result,
                "completeness": completeness,
                "shortest_trace": shortest_trace,
                "trace_count": trace_count,
            },
            create_defaults={
                "retries": 0,
                "result": result,
                "completeness": completeness,
                "shortest_trace": shortest_trace,
                "trace_count": trace_count,
            },
        )

    def fix_missing(self) -> None:
        # To find where missing checks are :
        # - first, we select theoric checks (a check per day per channel)
        # - then, we select actual checks stored in db
        # - finally, we make a difference between theoric and actual checks
        start = Check.objects.aggregate(min=Min("date"))["min"]
        end = pendulum.yesterday("utc").date()
        difference = Channel.objects.raw(
            """
            select id, period.day as day
            from quality_channel
            join (
                select date_trunc('day', generate_series(%s::date, %s, %s)) as day
            ) as period on True
            where end_date > period.day or end_date is null
            except
            select channel_id, date from quality_check
            """,
            [start, end, "1 day"],
        )
        missing_checks = [
            Check(channel_id=item.id, date=item.day) for item in difference
        ]
        Check.objects.bulk_create(missing_checks)

    def fix_closed_channels(self) -> None:
        # To find where checks for closed channels are :
        # - first, we select which cheks should be marked as closed
        # - then, we select actual checks for closed channels
        # - finally, we make a difference between theoric and actual checks
        start = Check.objects.aggregate(min=Min("date"))["min"]
        end = pendulum.yesterday("utc").date()
        difference = Channel.objects.raw(
            """
            select id, period.day as day
            from quality_channel
            join (select generate_series(%s::date, %s, %s) as day) as period on True
            where
                end_date <= period.day
                and extract(year from end_date) = extract(year from period.day)
            except
                select channel_id, date from quality_check where result = %s::integer
            """,
            [start, end, "1 day", Check.Result.CHANNEL_CLOSED],
        )
        closed_channel_checks = [
            Check(channel_id=item.id, date=item.day, result=Check.Result.CHANNEL_CLOSED)
            for item in difference
        ]
        Check.objects.bulk_create(
            closed_channel_checks,
            update_conflicts=True,
            update_fields=["result"],
            unique_fields=["channel_id", "date"],
        )


class CheckQuerySet(models.QuerySet["Check"]):
    def failed(self) -> "CheckQuerySet":
        return self.filter(
            retries__lt=CHECK_MAX_RETRIES,
            result__lte=Check.Result.DECONVOLUTION_FAILS,
        )

    def year(self, year: int | None = None) -> "CheckQuerySet":
        if year:
            requested_year = year
            period_filter = Q(date__year=requested_year)
        else:
            requested_year = pendulum.today("utc").year
            period_filter = Q(date__gte=pendulum.today("utc").subtract(months=1))
        return self.filter(
            period_filter,
            Q(channel__end_date=None) | Q(channel__end_date__year=requested_year),
            channel__station__start_date__year__lte=requested_year,
        )

    def networks(self, networks: list[str] | None = None) -> "CheckQuerySet":
        if networks:
            return self.filter(channel__station__network__code__in=networks)
        return self

    def stations(self, stations: list[str] | None = None) -> "CheckQuerySet":
        if stations:
            return self.filter(channel__station__code__in=stations)
        return self

    def channels(self, channels: list[str] | None = None) -> "CheckQuerySet":
        if channels:
            filters = Q()
            for channel in channels:
                filters |= Q(channel__code__istartswith=channel)
            return self.filter(filters)
        return self

    def operators(self, operators: list[str] | None = None) -> "CheckQuerySet":
        if operators:
            filters = Q()
            for operator in operators:
                filters |= Q(channel__station__operators__agency__icontains=operator)
            return self.filter(filters)
        return self

    def triggered(self, triggered: bool | None = None) -> "CheckQuerySet":
        if triggered is not None:
            return self.filter(channel__station__triggered=triggered)
        return self

    def summary(self) -> Any:
        return (
            self.select_related("channel__station__network")
            .values_list(
                "channel_id",
                "channel__code",
                "date",
                "completeness",
                "result",
                "trace_count",
                "shortest_trace",
                "channel__location",
                "channel__station__code",
                "channel__station__network__code",
            )
            .order_by(
                "-channel__station__network__code",
                "-channel__station__code",
                "-channel__location",
                "-channel__code",
                "date",
            )
        )


class Check(models.Model):
    class Result(models.IntegerChoices):
        NO_DATA = 0
        NOT_READABLE = 1
        DECONVOLUTION_FAILS = 2
        DECONVOLUTION_PASS = 3
        CHANNEL_CLOSED = 4

    channel = models.ForeignKey(Channel, models.CASCADE)
    date = models.DateField()
    result = models.IntegerField(choices=Result, default=Result.NO_DATA)
    retries = models.IntegerField(default=0)
    completeness = models.IntegerField(default=0)
    shortest_trace = models.IntegerField(default=0)
    trace_count = models.IntegerField(default=0)
    objects = CheckManager.from_queryset(CheckQuerySet)()

    class Meta:
        unique_together = (("channel", "date"),)

    def __str__(self) -> str:
        return f"{self.channel} - {self.date}"


class NetworkManager(models.Manager["Network"]):
    def populate(self, inventory: Inventory) -> None:
        values = [
            Network(code=network.code, description=network.description)
            for network in inventory.networks
        ]
        Network.objects.bulk_create(
            values,
            update_conflicts=True,
            update_fields=["description"],
            unique_fields=["code"],
        )


class Network(models.Model):
    code = models.CharField(unique=True, max_length=8)
    description = models.TextField(blank=True, default="")
    objects = NetworkManager()

    def __str__(self) -> str:
        return self.code


class StationManager(models.Manager["Station"]):
    def populate(self, inventory: Inventory) -> None:
        networks = {
            network.code: network.pk for network in Network.objects.only("pk", "code")
        }
        values = [
            Station(
                code=station.code,
                start_date=station.start_date.datetime.replace(tzinfo=datetime.UTC),
                end_date=None
                if station.end_date.datetime.replace(tzinfo=datetime.UTC)
                > pendulum.now("utc")
                else station.end_date.datetime.replace(tzinfo=datetime.UTC),
                network_id=networks[network.code],
            )
            for network in inventory.networks
            for station in network.stations
        ]
        Station.objects.bulk_create(
            values,
            update_conflicts=True,
            update_fields=["code", "start_date", "end_date", "network_id"],
            unique_fields=["code", "network_id"],
        )


class StationQuerySet(models.QuerySet["Station"]):
    def overview(self, network_id: int) -> Any:
        current_year = pendulum.today("utc").year
        one_week_ago = pendulum.today("utc").subtract(days=7)
        return (
            Station.objects.filter(
                Q(channel__end_date=None) | Q(channel__end_date__year=current_year),
                network_id=network_id,
                channel__check__date__gt=one_week_ago,
                start_date__year__lte=current_year,
            )
            .annotate(result=Min("channel__check__result"))
            .values("code", "id", "result")
            .order_by("code")
        )

    def opened(self, day: DateTime) -> "StationQuerySet":
        return self.filter(
            Q(start_date__lte=day),
            Q(end_date__isnull=True) | Q(end_date__gt=day),
        )

    def networks(self, networks: list[str] | None = None) -> "StationQuerySet":
        if networks:
            return self.filter(network__code__in=networks)
        return self

    def stations(self, stations: list[str] | None = None) -> "StationQuerySet":
        if stations:
            return self.filter(code__in=stations)
        return self

    def channels(self, channels: list[str] | None = None) -> "StationQuerySet":
        filters = Q()
        if channels:
            for channel in channels:
                filters |= Q(channel__code__istartswith=channel)
        return self.annotate(
            f_channels=FilteredRelation("channel", condition=filters),
        )

    def operators(self, operators: list[str] | None = None) -> "StationQuerySet":
        if operators:
            filters = Q()
            for operator in operators:
                filters |= Q(operators__agency__icontains=operator)
            return self.filter(filters)
        return self

    def year(self, year: int | None = None) -> "StationQuerySet":
        if year:
            requested_year = year
            period_filter = Q(f_channels__check__date__year=requested_year)
        else:
            requested_year = pendulum.today("utc").year
            period_filter = Q(
                f_channels__check__date__gte=pendulum.today("utc").subtract(months=1),
            )
        return self.filter(
            period_filter,
            Q(f_channels__end_date=None) | Q(f_channels__end_date__year=requested_year),
            start_date__year__lte=requested_year,
        )

    def triggered(self, triggered: bool | None = None) -> "StationQuerySet":
        if triggered is not None:
            return self.filter(triggered=triggered)
        return self

    def summary(self) -> Any:
        return (
            self.select_related("network")
            .annotate(
                completeness=Min("f_channels__check__completeness"),
                result=Min("f_channels__check__result"),
                trace_count=Max("f_channels__check__trace_count"),
                shortest_trace=Min("f_channels__check__shortest_trace"),
            )
            .values_list(
                "code",
                "pk",
                "f_channels__check__date",
                "completeness",
                "result",
                "trace_count",
                "shortest_trace",
                "network__code",
            )
            .order_by(
                "-network__code",
                "-code",
                "f_channels__check__date",
            )
        )


class Station(models.Model):
    code = models.CharField(max_length=8)
    network = models.ForeignKey(Network, models.CASCADE)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    triggered = models.BooleanField(default=False)
    objects = StationManager.from_queryset(StationQuerySet)()

    class Meta:
        unique_together = (("code", "network"),)

    def __str__(self) -> str:
        return f"{self.network}.{self.code}"

    def get_absolute_url(self) -> str:
        return reverse("quality:station-detail", kwargs={"pk": self.pk})


class OperatorManager(models.Manager["Operator"]):
    def populate(self, inventory: Inventory) -> None:
        operators = [
            Operator(agency=item)
            for item in {
                operator.agency
                for network in inventory.networks
                for station in network.stations
                for operator in station.operators
            }
        ]
        Operator.objects.bulk_create(
            operators,
            update_conflicts=True,
            update_fields=["website"],
            unique_fields=["agency"],
        )

    def link_to_stations(self, inventory: Inventory) -> None:
        operators = {
            operator.agency: operator.pk
            for operator in Operator.objects.only("pk", "agency")
        }
        stations = {
            network.code: {
                station.code: station.pk for station in network.station_set.all()
            }
            for network in Network.objects.prefetch_related("station_set").only(
                "code", "station__code"
            )
        }
        relationship = Station.operators.through
        relations = [
            relationship(
                station_id=stations[network.code][station.code],
                operator_id=operators[operator.agency],
            )
            for network in inventory.networks
            for station in network.stations
            for operator in station.operators
        ]
        relationship.objects.all().delete()
        relationship.objects.bulk_create(relations)

    def purge_obsoletes(self) -> None:
        self.filter(stations__isnull=True).delete()


class OperatorQuerySet(models.QuerySet["Operator"]):
    def acronyms_only(self) -> list[str]:
        return [
            operator.agency.upper().split("(")[1].split(")")[0]
            if "(" in operator.agency
            else operator.agency
            for operator in self
        ]

    def checks_exist(self) -> models.QuerySet["Operator"]:
        return self.filter(
            Exists(Check.objects.filter(channel=OuterRef("stations__channel"))),
        ).distinct()


class Operator(models.Model):
    agency = models.CharField(unique=True, max_length=255)
    website = models.URLField(blank=True)
    stations = models.ManyToManyField(Station, related_name="operators")
    objects = OperatorManager.from_queryset(OperatorQuerySet)()

    def __str__(self) -> str:
        return self.agency


# Maximum retries for each check
CHECK_MAX_RETRIES = 3

RESULT_PONDERATION = {
    Check.Result.NO_DATA: 50,
    Check.Result.NOT_READABLE: 100,
    Check.Result.DECONVOLUTION_FAILS: 150,
    Check.Result.DECONVOLUTION_PASS: 255,
    Check.Result.CHANNEL_CLOSED: 0,
}
