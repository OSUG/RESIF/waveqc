// Import our custom CSS
import "../scss/styles.scss";
import "tom-select/dist/css/tom-select.bootstrap5.min.css";

// plotly
// import Plotly from "plotly.js-cartesian-dist-min";

// Import only the Bootstrap components we need
// import { Popover } from 'bootstrap';

// Autocomplete field
import TomSelect from "tom-select/dist/js/tom-select.base";
import TomSelect_remove_button from "tom-select/dist/js/plugins/remove_button";
import TomSelect_clear_button from "tom-select/dist/js/plugins/clear_button";
import TomSelect_dropdown_header from "tom-select/dist/js/plugins/dropdown_header";

TomSelect.define("remove_button", TomSelect_remove_button);
TomSelect.define("clear_button", TomSelect_clear_button);
TomSelect.define("dropdown_header", TomSelect_dropdown_header);

document.querySelectorAll(".form-select").forEach((element) => {
  let settings = {
    maxOptions: null,
    plugins: {
      "remove_button": {},
      "clear_button": {},
      "dropdown_header": { title: element.attributes["placeholder"].value } },
  };
  new TomSelect(element, settings);
});
