from django import template
from django.http.request import QueryDict

from quality.models import Check

register = template.Library()

RESULT_COLORS = {
    Check.Result.NO_DATA: "danger",
    Check.Result.NOT_READABLE: "danger",
    Check.Result.DECONVOLUTION_FAILS: "warning",
    Check.Result.DECONVOLUTION_PASS: "success",
    Check.Result.CHANNEL_CLOSED: "dark",
}


@register.filter
def colorize(value: Check.Result) -> str:
    """
    Colorizes the result of a check
    """
    return RESULT_COLORS[value]


@register.filter
def getlist(query_dict: QueryDict, key: str) -> list[str]:
    """
    Returns a list of the data with the requested key.
    Returns an empty list if the key doesn't exist.
    """
    return query_dict.getlist(key)
