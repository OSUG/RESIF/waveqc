#############################################
FROM ghcr.io/astral-sh/uv:python3.12-bookworm-slim AS builder

# Enable bytecode compilation
# Copy from the cache instead of linking since it's a mounted volume
ENV UV_COMPILE_BYTECODE=1 \
    UV_LINK_MODE=copy

WORKDIR /app
COPY pyproject.toml uv.lock ./
# Sync only project dependencies
RUN --mount=type=cache,target=/root/.cache/uv \
    uv sync --frozen --no-install-project --no-dev

#############################################
FROM node:lts-alpine AS staticfiles

WORKDIR /app
COPY package.json package-lock.json .parcelrc ./
COPY quality/static_src/ ./quality/static_src/
RUN --mount=type=cache,target=/root/.npm npm install
RUN --mount=type=cache,target=/app/.parcel_cache npm run build

#############################################
FROM python:3.12-slim AS waveqc

RUN addgroup --system app && \
    adduser --system --group app && \
    mkdir -p app/staticfiles && \
    chown app:app app/staticfiles
USER app
WORKDIR /app
COPY --from=staticfiles /app/quality/static/ /app/quality/static/
COPY --from=builder --chown=app:app /app /app
COPY . .

# Keep these two lines after COPY . . statement
# There is a weird behaviour when building a new image :
# Build cache is not missed as expected and so the old commit sha is kept
# in the new release
ARG CI_COMMIT_SHORT_SHA
ENV SENTRY_RELEASE=$CI_COMMIT_SHORT_SHA \
    PATH="/app/.venv/bin:$PATH"

# For waveqc-web
# ENTRYPOINT ["gunicorn", "waveqc.wsgi", "--bind", "0.0.0.0:8000"]
# For waveqc-celery
# ENTRYPOINT ["celery", "-A", "waveqc", "worker"]
# For waveqc-celery-beat
# ENTRYPOINT ["celery", "-A", "waveqc", "beat"]
