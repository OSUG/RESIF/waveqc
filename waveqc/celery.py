import os

from celery import Celery
from celery.schedules import crontab
from django.conf import settings

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "waveqc.settings")
CHECKS_LAUNCH = settings.WAVEQC_SCHEDULE[0]
RETRY_LAUNCHES = settings.WAVEQC_SCHEDULE[2:]


app = Celery("waveqc")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.conf.beat_schedule = {
    "launch-checks-daily": {
        "task": "quality.tasks.launch_checks",
        "schedule": crontab(hour=CHECKS_LAUNCH, minute="15"),
        "args": (),
    },
    "retry-failing-checks-daily": {
        "task": "quality.tasks.retry_failing_checks",
        "schedule": crontab(hour=RETRY_LAUNCHES, minute="15"),
        "args": (),
    },
}
app.autodiscover_tasks()
